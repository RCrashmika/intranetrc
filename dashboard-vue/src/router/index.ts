import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import DocList from '../views/doc_flow/DocList.vue'
import AmazonAffiliate from '../views/doc_flow/Amazon Affiliate.vue'
import CustomFilterExperimental from '../views/doc_flow/CustomFilterExperimental.vue'
import MyKSFOpenCost from '../views/doc_flow/MyKSFOpenCost.vue'
import MyBLUEOpenCost from '../views/doc_flow/MyBLUEOpenCost.vue'
import MyECSOpenCost from '../views/doc_flow/MyECSOpenCost.vue'
import MyIMMOOpenCost from '../views/doc_flow/MyIMMOOpenCost.vue'
import MyOpenDocuments from '../views/doc_flow/MyOpenDocuments.vue'
import MyRedcorpOpenCost from '../views/doc_flow/MyRedcorpOpenCost.vue'
import ProcessOpenInvoices from '../views/doc_flow/ProcessOpenInvoices.vue'
import ProcessOpenWMPurchInc from '../views/doc_flow/ProcessOpenWMPurchInc.vue'
import ProcessOpenDocs from '../views/doc_flow/ProcessOpenDocs.vue'
import ProcessDFLIC from '../views/doc_flow/ProcessDFLIC.vue'
import ProcessOpenCreditNotes from '../views/doc_flow/ProcessOpenCreditNotes.vue'
import ProcessOpenCustDoc from '../views/doc_flow/ProcessOpenCustDoc.vue'

import AllCustOrderList_epp from '../views/epp/AllCustOrderList.vue'
import OpenProductStatus_epp from '../views/epp/OpenProductStatus.vue'
import OrderToFulfill_epp from '../views/epp/OrderToFulfill.vue'
import OverOrder_epp from '../views/epp/OverOrder.vue'
import OpenProductStatus_ogp from '../views/ogp/OpenProductStatus.vue'
import OrderToFulfill_ogp from '../views/ogp/OverOrder.vue'
import OverOrder_ogp from '../views/ogp/OverOrder.vue'
import CustomersBO from '../views/sales/CustomersBO.vue'
import PackagedToday from '../views/sales/PackagedToday.vue'
import ThirdPartiesReport from '../views/sales/ThirdPartiesReport.vue'
import TNTRates from '../views/sales/TNTRates.vue'

import Login from '../views/Login.vue'
import Blank from '../views/BlankView.vue'
import NotFound from '../views/NotFound.vue'
import Forget from '../views/Forget.vue'
import Reset from '../views/Reset.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: { layout: 'empty' },
  },
  {
    path: '/forget',
    name: 'Forget',
    component: Forget,
    meta: { layout: 'empty' },
  },
  {
    path: '/reset',
    name: 'Reset',
    component: Reset,
    meta: { layout: 'empty' },
  },
  {
    path: '/doclist',
    name: 'DocList',
    component: DocList,
  },
  {
    path: '/amazonAffiliate',
    name: 'Amazon Affiliate',
    component: AmazonAffiliate,
  },
  {
    path: '/customFilterExperimental',
    name: 'CustomFilterExperimental',
    component: CustomFilterExperimental,
  },
  {
    path: '/myKSFOpenCost',
    name: 'MyKSFOpenCost',
    component: MyKSFOpenCost,
  },
  {
    path: '/myBLUEOpenCost',
    name: 'MyBLUEOpenCost',
    component: MyBLUEOpenCost,
  },
  {
    path: '/myECSOpenCost',
    name: 'MyECSOpenCost',
    component: MyECSOpenCost,
  },
  {
    path: '/myIMMOOpenCost',
    name: 'MyIMMOOpenCost',
    component: MyIMMOOpenCost,
  },
  {
    path: '/myOpenDocuments',
    name: 'MyOpenDocuments',
    component: MyOpenDocuments,
  },
  {
    path: '/myRedcorpOpenCost',
    name: 'MyRedcorpOpenCost',
    component: MyRedcorpOpenCost,
  },
  {
    path: '/processOpenInvoices',
    name: 'ProcessOpenInvoices',
    component: ProcessOpenInvoices,
  },
  {
    path: '/processOpenWMPurchInc',
    name: 'ProcessOpenWMPurchInc',
    component: ProcessOpenWMPurchInc,
  },
  {
    path: '/processOpenDocs',
    name: 'ProcessOpenDocs',
    component: ProcessOpenDocs,
  },
  {
    path: '/processDFLIC',
    name: 'ProcessDFLIC',
    component: ProcessDFLIC,
  },
  {
    path: '/processOpenCreditNotes',
    name: 'ProcessOpenCreditNotes',
    component: ProcessOpenCreditNotes,
  },
  {
    path: '/processOpenCustDoc',
    name: 'ProcessOpenCustDoc',
    component: ProcessOpenCustDoc,
  },
  {
    path: '/allCustOrderList_epp',
    name: 'AllCustOrderList_epp',
    component: AllCustOrderList_epp,
  },
  {
    path: '/openProductStatus_epp',
    name: 'OpenProductStatus_epp',
    component: OpenProductStatus_epp,
  },
  {
    path: '/orderToFulfill_epp',
    name: 'OrderToFulfill_epp',
    component: OrderToFulfill_epp,
  },
  {
    path: '/overOrder_epp',
    name: 'ProcessOpOverOrder_eppenCustDoc',
    component: OverOrder_epp,
  },
  {
    path: '/openProductStatus_ogp',
    name: 'OpenProductStatus_ogp',
    component: OpenProductStatus_ogp,
  },
  {
    path: '/orderToFulfill_ogp',
    name: 'OrderToFulfill_ogp',
    component: OrderToFulfill_ogp,
  },
  {
    path: '/overOrder_ogp',
    name: 'OverOrder_ogp',
    component: OverOrder_ogp,
  },
  {
    path: '/customersBO',
    name: 'CustomersBO',
    component: CustomersBO,
  },
  {
    path: '/packagedToday',
    name: 'PackagedToday',
    component: PackagedToday,
  },
  {
    path: '/thirdPartiesReport',
    name: 'ThirdPartiesReport',
    component: ThirdPartiesReport,
  },
  {
    path: '/tNTRates',
    name: 'TNTRates',
    component: TNTRates,
  },
  {
    path: '/blank',
    name: 'Blank',
    component: Blank,
  },
  { path: '/:pathMatch(.*)*', component: NotFound, meta: { layout: 'empty' }, },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
